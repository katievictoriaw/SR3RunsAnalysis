#include "CutsSR3RunsAnalysis.h"
#include "ConfigSvc.h"

CutsSR3RunsAnalysis::CutsSR3RunsAnalysis(SR3RunsAnalysisEvent* SR3RunsAnalysis_Event)
{
    m_evt = SR3RunsAnalysis_Event;
}

CutsSR3RunsAnalysis::~CutsSR3RunsAnalysis()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsSR3RunsAnalysis::SR3RunsAnalysisCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
