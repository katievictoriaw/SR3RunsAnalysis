#include "SR3RunsAnalysisEvent.h"

SR3RunsAnalysisEvent::SR3RunsAnalysisEvent(EventBase* base): 

  runID(base->m_reader, "eventHeader.runID"),
  eventID(base->m_reader, "eventHeader.eventID"),
  triggerTimeStamp_s(base->m_reader, "eventHeader.triggerTimeStamp_s"),
  triggerTimeStamp_ns(base->m_reader, "eventHeader.triggerTimeStamp_ns"),
  daq_livetime_s(base->m_reader, "eventHeader.daqLivetime_s"),
  daq_livetime_ns(base->m_reader, "eventHeader.daqLivetime_ns"),
  trigger_livetime_s(base->m_reader, "eventHeader.triggerLivetime_s"),
  trigger_livetime_ns(base->m_reader, "eventHeader.triggerLivetime_ns"),
  physics_livetime_s(base->m_reader, "eventHeader.physicsLivetime_s"),
  physics_livetime_ns(base->m_reader, "eventHeader.physicsLivetime_ns"),

  SS_nSingleScatters(base->m_reader, "ss.nSingleScatters"),
  SS_s1PulseID(base->m_reader, "ss.s1PulseID"),
  SS_s2PulseID(base->m_reader, "ss.s2PulseID"),
  SS_s1Area(base->m_reader, "ss.s1Area_phd"),
  SS_s2Area(base->m_reader, "ss.s2Area_phd"),
  SS_correctedS1Area(base->m_reader, "ss.correctedS1Area_phd"),
  SS_correctedS2Area(base->m_reader, "ss.correctedS2Area_phd"),
  SS_s2BottomArea(base->m_reader, "ss.s2BottomArea_phd"),
  SS_s2TopArea(base->m_reader, "ss.s2TopArea_phd"),
  SS_correctedS2BottomArea(base->m_reader, "ss.correctedS2BottomArea_phd"),
  SS_correctedS2TopArea(base->m_reader, "ss.correctedS2TopArea_phd"),
  SS_driftTime_ns(base->m_reader, "ss.driftTime_ns"),
  SS_X_cm(base->m_reader, "ss.x_cm"),
  SS_Y_cm(base->m_reader, "ss.y_cm"),
  SS_cX_cm(base->m_reader, "ss.correctedX_cm"),
  SS_cY_cm(base->m_reader, "ss.correctedY_cm"),
  SS_cZ_cm(base->m_reader, "ss.correctedZ_cm"),
  SS_s1TBA(base->m_reader, "ss.s1TBA")

{
}

SR3RunsAnalysisEvent::~SR3RunsAnalysisEvent()
{
}
