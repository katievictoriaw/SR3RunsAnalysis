#include "SR3RunsAnalysis.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsSR3RunsAnalysis.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
SR3RunsAnalysis::SR3RunsAnalysis()
    : Analysis()
    , m_evt(new SR3RunsAnalysisEvent(m_event))
{
    m_event->Initialize();
    // test comment
    // Setup logging
    logging::set_program_name("SR3RunsAnalysis");
    // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5

    m_cutsSR3RunsAnalysis = new CutsSR3RunsAnalysis(m_evt);

    //create a config instance, this can be used to call the config variables.
    m_conf = ConfigSvc::Instance();
}

// Destructor
SR3RunsAnalysis::~SR3RunsAnalysis()
{
    delete m_cutsSR3RunsAnalysis;
}

// Called before event loop
void SR3RunsAnalysis::Initialize()
{
    INFO("Initializing SR3RunsAnalysis");
    
    // Initialise SR1 core cuts
    m_cuts->sr1()->Initialize();
    m_cuts->sr1()->SetHotspotExclusions();
    m_cuts->sr1()->SetS1RateSpikeExclusions();
    
    // Initialise SR3 core cuts
    m_cuts->sr3()->Initialize();
    m_cuts->sr3()->SetHotspotExclusions();
    m_cuts->sr3()->SetS1RateSpikeExclusions();
    m_cuts->sr3()->InitializeETrainVetoCustomization(true, "fullTPC", 7.25, 1.40, 5.81, "ByPulses", "loglog");
    
    // Define SS tree and branches
    auto SS_tree = std::make_tuple(std::make_pair("runID", &runID),
                                   std::make_pair("eventID", &eventID),
                                   std::make_pair("triggerTimeStamp_s", &triggerTimeStamp_s),
                                   std::make_pair("triggerTimeStamp_ns", &triggerTimeStamp_ns),
                                   std::make_pair("daq_livetime", &daq_livetime),
                                   std::make_pair("trigger_livetime", &trigger_livetime),
                                   std::make_pair("physics_livetime", &physics_livetime),
                                   std::make_pair("SS_raw_S1", &SS_raw_S1),
                                   std::make_pair("SS_raw_S2", &SS_raw_S2),
                                   std::make_pair("SS_raw_S2_top", &SS_raw_S2_top),
                                   std::make_pair("SS_raw_S2_bot", &SS_raw_S2_bot),
                                   std::make_pair("SS_S1c", &SS_S1c),
                                   std::make_pair("SS_S2c", &SS_S2c),
                                   std::make_pair("SS_S2c_top", &SS_S2c_top),
                                   std::make_pair("SS_S2c_bot", &SS_S2c_bot),
                                   std::make_pair("SS_driftTime_us", &SS_driftTime_us),
                                   std::make_pair("SS_X_cm", &SS_X_cm),
                                   std::make_pair("SS_Y_cm", &SS_Y_cm),
                                   std::make_pair("SS_R_cm", &SS_R_cm),
                                   std::make_pair("SS_cX_cm", &SS_cX_cm),
                                   std::make_pair("SS_cY_cm", &SS_cY_cm),
                                   std::make_pair("SS_cR_cm", &SS_cR_cm),
                                   std::make_pair("SS_cZ_cm", &SS_cZ_cm),
                                   std::make_pair("SS_s1TBA", &SS_s1TBA),
                                   std::make_pair("cxSR1_HSX", &cxSR1_HSX),
                                   std::make_pair("cxSR1_SPEX", &cxSR1_SPEX),
                                   std::make_pair("cxSR1_S1rate", &cxSR1_S1rate),
                                   std::make_pair("cxSR1_muonV", &cxSR1_muonV),
                                   std::make_pair("cxSR1_EA", &cxSR1_EA),
                                   std::make_pair("cxSR1_HSC", &cxSR1_HSC),
                                   std::make_pair("cxSR1_stinger", &cxSR1_stinger),
                                   std::make_pair("cxSR1_S1shape", &cxSR1_S1shape),
                                   std::make_pair("cxSR1_S1prom", &cxSR1_S1prom),
                                   std::make_pair("cxSR1_S1timing", &cxSR1_S1timing),
                                   std::make_pair("cxSR1_S1TBA", &cxSR1_S1TBA),
                                   std::make_pair("cxSR1_S2TBA", &cxSR1_S2TBA),
                                   std::make_pair("cxSR1_validXY", &cxSR1_validXY),
                                   std::make_pair("cxSR1_S2width", &cxSR1_S2width),
                                   std::make_pair("cxSR1_narrowS2", &cxSR1_narrowS2),
                                   std::make_pair("cxSR1_S2earlypeak", &cxSR1_S2earlypeak),
                                   std::make_pair("cxSR1_S2risetime", &cxSR1_S2risetime),
                                   std::make_pair("cxSR1_FCRXY", &cxSR1_FCRXY),
                                   std::make_pair("cxSR1_FV", &cxSR1_FV),
                                   std::make_pair("cxSR1_burst_noise", &cxSR1_burst_noise),
                                   std::make_pair("cxSR1_buffer", &cxSR1_buffer),
                                   std::make_pair("cxSR1_skinV", &cxSR1_skinV),
                                   std::make_pair("cxSR1_skinVprompt", &cxSR1_skinVprompt),
                                   std::make_pair("cxSR1_skinVdel", &cxSR1_skinVdel),
                                   std::make_pair("cxSR1_ODV", &cxSR1_ODV),
                                   std::make_pair("cxSR1_ODVprompt", &cxSR1_ODVprompt),
                                   std::make_pair("cxSR1_ODVdel", &cxSR1_ODVdel),
                                   std::make_pair("cxSR3_HSX", &cxSR3_HSX),
                                   std::make_pair("cxSR3_SPEX", &cxSR3_SPEX),
                                   std::make_pair("cxSR3_S1rate", &cxSR3_S1rate),
                                   std::make_pair("cxSR3_muonV", &cxSR3_muonV),
                                   std::make_pair("cxSR3_ETV", &cxSR3_ETV),
                                   std::make_pair("cxSR3_HSC", &cxSR3_HSC),
                                   std::make_pair("cxSR3_stinger", &cxSR3_stinger),
                                   std::make_pair("cxSR3_S1TBA", &cxSR3_S1TBA),
                                   std::make_pair("cxSR3_S2TBA", &cxSR3_S2TBA),
                                   std::make_pair("cxSR3_S2XYquality", &cxSR3_S2XYquality),
                                   std::make_pair("cxSR3_S2width", &cxSR3_S2width),
                                   std::make_pair("cxSR3_narrowS2", &cxSR3_narrowS2),
                                   std::make_pair("cxSR3_S2earlypeak", &cxSR3_S2earlypeak),
                                   std::make_pair("cxSR3_S2risetime", &cxSR3_S2risetime),
                                   std::make_pair("cxSR3_FCRXY", &cxSR3_FCRXY),
                                   std::make_pair("cxSR3_burst_noise", &cxSR3_burst_noise),
                                   std::make_pair("cxSR3_buffer", &cxSR3_buffer),
                                   std::make_pair("cxSR3_SSwindow", &cxSR3_SSwindow),
                                   std::make_pair("cxSR3_skinV", &cxSR3_skinV),
                                   std::make_pair("cxSR3_skinVprompt", &cxSR3_skinVprompt),
                                   std::make_pair("cxSR3_skinVdel", &cxSR3_skinVdel),
                                   std::make_pair("cxSR3_ODV", &cxSR3_ODV),
                                   std::make_pair("cxSR3_ODVprompt", &cxSR3_ODVprompt),
                                   std::make_pair("cxSR3_ODVdel", &cxSR3_ODVdel));
    
    // Book TTrees
    m_hists->BookTree("SS", SS_tree);
}

// Called once per event
void SR3RunsAnalysis::Execute()
{
    // Cut initialisation steps
    m_cuts->sr3()->InitializeETrainVetoInEvent(SR3CoreCuts::physics_livetime);
    
    // General event information
    runID = *m_evt->runID;
    eventID = *m_evt->eventID;
    triggerTimeStamp_s = *m_evt->triggerTimeStamp_s;
    triggerTimeStamp_ns = *m_evt->triggerTimeStamp_ns;
    
    // Livetime information
    daq_livetime = (*m_evt->daq_livetime_s) + 1e-9*(*m_evt->daq_livetime_ns);
    trigger_livetime = (*m_evt->trigger_livetime_s) + 1e-9*(*m_evt->trigger_livetime_ns);
    physics_livetime = (*m_evt->physics_livetime_s) + 1e-9*(*m_evt->physics_livetime_ns);
    ETV_deadtime = m_cuts->sr3()->GetLivetimeVetoedByETrainThisEvent() * 1e-9;
    
    // Select SS events
    if (*m_evt->SS_nSingleScatters > 0)
    {
        
        // Pulse IDs
        int S1_ID = *m_evt->SS_s1PulseID;
        int S2_ID = *m_evt->SS_s2PulseID;
        
        // Position information
        SS_X_cm = *m_evt->SS_X_cm;
        SS_Y_cm = *m_evt->SS_Y_cm;
        SS_cX_cm = *m_evt->SS_cX_cm;
        SS_cY_cm = *m_evt->SS_cY_cm;
        SS_cZ_cm = *m_evt->SS_cZ_cm;
        SS_driftTime_us = *m_evt->SS_driftTime_ns / 1e3;
        SS_cR_cm = std::sqrt(std::pow(SS_X_cm, 2) + std::pow(SS_Y_cm, 2));
        SS_cR_cm = std::sqrt(std::pow(SS_X_cm, 2) + std::pow(SS_Y_cm, 2));
        SS_s1TBA = *m_evt->SS_s1TBA;
        
        // Raw pulse areas
        SS_raw_S1 = *m_evt->SS_s1Area;
        SS_raw_S2 = *m_evt->SS_s2Area;
        SS_raw_S2_top = *m_evt->SS_s2TopArea;
        SS_raw_S2_bot = *m_evt->SS_s2BottomArea;
        
        // Corrected pulse areas
        SS_S1c = *m_evt->SS_correctedS1Area;
        SS_S2c = *m_evt->SS_correctedS2Area;
        SS_S2c_top = *m_evt->SS_correctedS2TopArea;
        SS_S2c_bot = *m_evt->SS_correctedS2BottomArea;
        
        // SR1 core cuts
        cxSR1_HSX = m_cuts->sr1()->HotspotExclusion();
        cxSR1_SPEX = m_cuts->sr1()->PassSustainedSPERateCut();
        cxSR1_S1rate = m_cuts->sr1()->S1RateSpikeExclusion();
        bool cxSR1_IsMuon = m_cuts->sr1()->IsTPCMuon(); // must be called every event for TPCMuonVeto() to work
        cxSR1_muonV = m_cuts->sr1()->TPCMuonVeto();
        cxSR1_EA = m_cuts->sr1()->PassExcessArea();
        cxSR1_HSC = m_cuts->sr1()->PassHSCCut(S1_ID);
        cxSR1_stinger = m_cuts->sr1()->Stinger(S1_ID);
        cxSR1_S1shape = m_cuts->sr1()->PassS1ShapeCut(S1_ID);
        cxSR1_S1prom = m_cuts->sr1()->ProminenceCut_SS();
        cxSR1_S1timing = m_cuts->sr1()->PassS1ChannelTiming(S1_ID);
        cxSR1_S1TBA = m_cuts->sr1()->S1TBA_SS();
        cxSR1_S2TBA = m_cuts->sr1()->GasS2TBACut();
        cxSR1_validXY = m_cuts->sr1()->IsValidXY(S2_ID);
        cxSR1_S2width = m_cuts->sr1()->S2Width_SS();
        cxSR1_narrowS2 = m_cuts->sr1()->NarrowS2Cut();
        cxSR1_S2earlypeak = m_cuts->sr1()->S2EarlyPeakCut();
        cxSR1_S2risetime = m_cuts->sr1()->RiseTimeCut();
        cxSR1_FCRXY = m_cuts->sr1()->FieldCageResistorCut();
        cxSR1_FV = m_cuts->sr1()->Fiducial();
        cxSR1_burst_noise = m_cuts->sr1()->PassODBurstNoiseTagSS(S1_ID, S2_ID);
        cxSR1_buffer = m_cuts->sr1()->BufferStartTime() && m_cuts->sr1()->BufferStopTime(S1_ID, S2_ID);
        cxSR1_skinV = m_cuts->sr1()->SkinVeto(S1_ID);
        cxSR1_skinVprompt = m_cuts->sr1()->SkinPromptVeto(S1_ID);
        cxSR1_skinVdel = m_cuts->sr1()->SkinDelayedVeto(S1_ID);
        cxSR1_ODV = m_cuts->sr1()->ODVeto(S1_ID);
        cxSR1_ODVprompt = m_cuts->sr1()->ODPromptVeto(S1_ID);
        cxSR1_ODVdel = m_cuts->sr1()->ODDelayedVeto(S1_ID);
        
        // SR3 core cuts
        cxSR3_HSX = m_cuts->sr3()->HotspotExclusion();
        cxSR3_SPEX = m_cuts->sr3()->SustainedSPERate();
        cxSR3_S1rate = m_cuts->sr3()->S1RateSpikeExclusion();
        bool cxSR3_IsMuon = m_cuts->sr3()->IsTPCMuonHighRate(); // Must be called every event for TPCMuonHoldOff() to work.
        cxSR3_muonV = m_cuts->sr3()->TPCMuonHoldOff();
        cxSR3_ETV = m_cuts->sr3()->PassesETrainVeto(S1_ID, "S2");
        cxSR3_HSC = m_cuts->sr3()->S1HighSingleChannel(S1_ID);
        cxSR3_stinger = m_cuts->sr3()->S1Stinger(S1_ID);
        cxSR3_S1TBA = m_cuts->sr3()->S1TBAVsDriftTime();
        cxSR3_S2TBA = m_cuts->sr3()->S2TBACut();
        cxSR3_S2XYquality = m_cuts->sr3()->S2XYQuality(S2_ID);
        cxSR3_S2width = m_cuts->sr3()->S2WidthVsDriftTime();
        cxSR3_narrowS2 = m_cuts->sr3()->S2NarrowWidth();
        cxSR3_S2earlypeak = m_cuts->sr3()->S2EarlyPeak();
        cxSR3_S2risetime = m_cuts->sr3()->S2RiseTime();
        cxSR3_FCRXY = m_cuts->sr3()->FieldCageResistorCut();
        cxSR3_burst_noise = m_cuts->sr3()->PassODBurstNoiseTagSS(S1_ID, S2_ID);
        cxSR3_buffer = m_cuts->sr3()->BufferCut();
        cxSR3_SSwindow = m_cuts->sr3()->SSWindowS1() && m_cuts->sr3()->SSWindowS2();
        cxSR3_skinV = m_cuts->sr3()->SkinVeto(S1_ID);
        cxSR3_skinVprompt = m_cuts->sr3()->SkinPromptVeto(S1_ID);
        cxSR3_skinVdel = m_cuts->sr3()->SkinDelayedVeto(S1_ID);
        cxSR3_ODV = m_cuts->sr3()->ODVeto(S1_ID);
        cxSR3_ODVprompt = m_cuts->sr3()->ODPromptVeto(S1_ID);
        cxSR3_ODVdel = m_cuts->sr3()->ODDelayedVeto(S1_ID);
        
        // Fill TTree
        m_hists->FillTree("SS");
    }
    
    // Cut finalisation steps
    m_cuts->sr3()->DeleteInactiveProgenitors();
}

// Called after event loop
void SR3RunsAnalysis::Finalize()
{
}
