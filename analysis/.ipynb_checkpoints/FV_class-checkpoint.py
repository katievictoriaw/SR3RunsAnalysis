# ------------------------------------------------------------------
# Class of supplementary functions for FV_studies.ipynb
# - data structures, fitting, plotting, and more.
#
# Author: Aiham Al Musalhi
# ------------------------------------------------------------------

import os, re
import time
import datetime
import pickle
import uproot
import imageio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplc
from matplotlib import gridspec
from matplotlib.path import Path
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit, fmin, minimize
from dateutil import tz
from numba import jit, njit

# Increase font size of plots
plt.rcParams.update({'font.size': 13.5})

# Make all figures have a white background by default
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 1),
    "axes.facecolor":    (1.0, 1.0, 1.0, 1),
    "savefig.facecolor": (1.0, 1.0, 1.0, 1),
})

class toolbox:
    
    # Global variables
    dir_path = '/global/cfs/projectdirs/lz/users/almusalhi/analysis/'
    ddir = dir_path + 'data/' # input data storage
    fdir = dir_path + 'FV_studies/' # working directory
    
    # Constructor
    def __init__(self):
        
        # Energy reconstruction parameters (from Greg B.'s Doke plot)
        self.W_keV = 13.5e-3 # [keV]
        self.g1 = 0.1148 # [photons/phd]
        self.g2 = 36.23 # [photoelectrons/phd]
        self.g2_bot = 11.52 # [photoelectrons/phd]
        
        # SR3 WIMP ROI
        self.S1c_bounds = [0, 120] # [phd]
        self.logS2c_bounds = [2.378, 4.5] # [phd] ~5 electrons min.
        
        # General parameters
        self.max_drift = 1046 # [us]
        
        # Load ER and NR bands, and perform interpolation
        self.load_skew_bands(stype = 'linear')
        
        
    # -------------------- Data loading methods --------------------
    
    def load_data(self, fname, recreate = False):
        
        """
        Loads SS events from TTrees produced by the SR3RunsAnalysis
        module, and formats them into a dictionary structure.
        A new pickle file is generated if not already present.
        """
        
        # Load data from pickle file if it exists
        if (fname + '.pkl') in os.listdir(self.ddir + 'pkl_files/') and recreate == False:
            with open(self.ddir + f'pkl_files/{fname}.pkl', 'rb') as f:
                data = pickle.load(f)
            return data
        
        # Otherwise, load data from .root file
        else:

            # Load .root file and dump into dictionary
            f = uproot.open(self.ddir + f'root_files/{fname}.root:SS')
            data = {k:f[k].array(library = 'np') for k in f.keys()}
            f.close()
            
            # Add SR3 WIMP ROI as a cut
            data['cxSR3_WIMP_ROI'] = (data['SS_S1c'] > self.S1c_bounds[0]) \
                                   & (data['SS_S1c'] <= self.S1c_bounds[1]) \
                                   & (np.log10(data['SS_S2c']) > self.logS2c_bounds[0]) \
                                   & (np.log10(data['SS_S2c']) <= self.logS2c_bounds[1])


            # Write out pickle file and return data structure
            with open(self.ddir + f'pkl_files/{fname}.pkl', 'wb') as handle:
                pickle.dump(data, handle, protocol = pickle.HIGHEST_PROTOCOL)
            return data
    
    # ---------------------- Plotting methods ----------------------

    def plot_data_summary(self, data, basic_cuts = None, extra_cuts = None,
                          nbins = 200, S1c_range = (0, 120), S2c_range = (10**1.5, 10**5),
                          dt_range = (-1150e-3, 50e-3), R2_range = (0, 74**2), 
                          preserve_S1S2 = False, title = '', cutpoint_alpha = 0.7,
                          cutpoint_c = 'r', cmap = 'cividis', draw_bands = True,
                          cbmax = None, logx = False, logy = True, save_str = None,
                          return_axes = False):
        
        """
        For a given dataset dictionary, plots a generic overview
        of drift-R^2 space, XY space, and S1c-S2c space.
        
        Inputs:
        - data: input dataset in dictionary form
        - basic_cuts: boolean array for main cut to apply for all plots
        - extra_cuts: comparative cut, the effect of which is overlaid
        - nbins: number of bins in all 2d histograms produced
        - S1c_range: axis and binning range of S1c values
        - S2c_range: axis and binning range of S2c values
        - dt_range: axis and binning range of drift time values
        - R2_range: axis and binning range of R^2 values
        - preserve_S1S2: basic_cuts applied to S1c-S2c plot if False
        - title: overall figure title string
        - cutpoint_alpha: alpha for points removed by extra_cuts
        - cutpoint_c: colour of points removed by extra_cuts
        - cmap: colour map scheme
        - draw_bands: overlay ER and NR bands
        - cbmax: upper limit of colour bar
        - logx: True for log10(S1c)
        - logy: True for log10(S2c)
        - save_str: name for saved plot
        - return_axes: output the figure and axes
        """
        
        # Evaluate appropriate cut booleans
        default = np.ones(len(data['SS_S1c'])).astype(bool)
        combined_cut = (default if basic_cuts is None else basic_cuts) \
                     & (default if extra_cuts is None else extra_cuts)
        cut_rejection = (default if basic_cuts is None else basic_cuts) \
                     & ~(default if extra_cuts is None else extra_cuts)
        
        # Create generic exploration figure
        fig = plt.figure(figsize = (8.2, 10))
        cbar_ax = fig.add_axes([0.93, 0.123, 0.03, 0.7575]) # axis for shared colour bar
        gs = gridspec.GridSpec(2, 2, figure = fig, height_ratios = [2, 3], width_ratios = [1, 0.95])
        ax0, ax1 = plt.subplot(gs[0, 0]), plt.subplot(gs[0, 1])
        ax2 = plt.subplot(gs[1, :])

        # drift-R^2 plot
        h0 = ax0.hist2d(data['SS_R_cm'][combined_cut]**2, -data['SS_driftTime_us'][combined_cut]*1e-3,
                        bins = nbins, range = (R2_range, dt_range), cmin = 1,
                        cmap = cmap, norm = mplc.LogNorm(vmax = cbmax))

        # XY plot
        h1 = ax1.hist2d(data['SS_X_cm'][combined_cut], data['SS_Y_cm'][combined_cut],
                        bins = nbins, range = ((-80, 80), (-80, 80)), cmin = 1, cmap = cmap,
                        norm = mplc.LogNorm(vmax = np.max(h0[0][~np.isnan(h0[0])]) \
                                            if cbmax is None else cbmax))

        # S1c-S2c plot
        xbins = 10**np.linspace(np.log10(S1c_range[0] if S1c_range[0] > 0 else 1e0),
                                np.log10(S1c_range[1]), nbins + 1) \
                if logx else np.linspace(S1c_range[0], S1c_range[1], nbins + 1)
        ybins = 10**np.linspace(np.log10(S2c_range[0] if S2c_range[0] > 0 else 1e2),
                                np.log10(S2c_range[1]), nbins + 1) \
                if logy else np.linspace(S2c_range[0], S2c_range[1], nbins + 1)
        h2 = ax2.hist2d(data['SS_S1c'][default if preserve_S1S2 else combined_cut],
                        data['SS_S2c'][default if preserve_S1S2 else combined_cut],
                        bins = (xbins, ybins), cmin = 1, cmap = cmap,
                        norm = mplc.LogNorm(vmax = np.max(h0[0][~np.isnan(h0[0])]) \
                                            if cbmax is None else cbmax))

        # Plot events removed by cut
        if extra_cuts is not None:
            ax0.plot(data['SS_R_cm'][cut_rejection]**2, -data['SS_driftTime_us'][cut_rejection]*1e-3,
                     'rx', color = cutpoint_c, alpha = cutpoint_alpha)
            ax1.plot(data['SS_X_cm'][cut_rejection], data['SS_Y_cm'][cut_rejection],
                     'rx', color = cutpoint_c, alpha = cutpoint_alpha)
            ax2.plot(data['SS_S1c'][~extra_cuts if preserve_S1S2 else cut_rejection],
                     data['SS_S2c'][~extra_cuts if preserve_S1S2 else cut_rejection],
                     'rx', color = cutpoint_c, alpha = cutpoint_alpha, label = 'Rejected points')
            ax2.legend(loc = 'center', bbox_to_anchor = (0.5, 1.075), framealpha = 1)
            
        # Overlay ER and NR bands
        if draw_bands:
            
            # ER band
            ax2.plot(self.ER_band[:, 0], 10**self.ER_band[:, 1], 'c-', lw = 1.5)
            ax2.plot(self.ER_band[:, 0], 10**self.ER_band[:, 2], 'c--', lw = 1.5)
            ax2.plot(self.ER_band[:, 0], 10**self.ER_band[:, 3], 'c--', lw = 1.5)
            
            # NR band
            ax2.plot(self.NR_band[:, 0], 10**self.NR_band[:, 1], 'c-',
                     color = 'lightcoral', lw = 1.5)
            ax2.plot(self.NR_band[:, 0], 10**self.NR_band[:, 2], 'c--',
                     color = 'lightcoral', lw = 1.5)
            ax2.plot(self.NR_band[:, 0], 10**self.NR_band[:, 3], 'c--',
                     color = 'lightcoral', lw = 1.5)

        # Formatting
        cb = fig.colorbar(h0[3], cax = cbar_ax)
        cb.set_label('SS counts')
        ax0.set_xlabel(r'R$^2$ [cm$^2$]')
        ax0.set_ylabel(r'-Drift time [ms]')
        ax0.set_xticks(np.array([10, 30, 40, 50, 60, 70])**2)
        ax0.set_xticklabels([f'{r}' + r'$^{2}$' for r in [10, 30, 40, 50, 60, 70]])
        ax1.set_xlabel('x [cm]')
        ax1.set_ylabel('y [cm]', labelpad = -10)
        ax1.set_xticks([-80, -40, 0, 40, 80])
        ax1.set_yticks([-80, -40, 0, 40, 80])
        ax2.set_xlabel('S1c [phd]')
        ax2.set_ylabel('S2c [phd]')
        ax2.set_xscale('log' if logx else 'linear')
        ax2.set_yscale('log' if logy else 'linear')
        ax2.set_xlim(S1c_range[0] if S1c_range[0] > 0 else 1e0, S1c_range[1])
        ax2.set_ylim(S2c_range[0] if S2c_range[0] > 0 else 1e2, S2c_range[1])
        fig.text(0.5, 0.91, title, ha = 'center', va = 'center', fontsize = 15)
        plt.subplots_adjust(wspace = 0.3, hspace = 0.25)
        if save_str is not None:
            plt.savefig(self.fdir + f'plots/{save_str}.png', dpi = 300, bbox_inches = 'tight')
            
        # Return axes
        if return_axes:
            return (fig, ax0, ax1, ax2)
            
    def plot_heatmap(self, zvals, xslices, yslices, xlabel, ylabel, cblabel,
                     cmap = 'viridis', logz = False, title = None):

        """
        Produces a heat map given a set of (x, y) bin edges and
        weights for a colour map. Plot labels must be provided as well.
        """

        # Get tile centres, generate a meshgrid, then unravel
        xdata = 0.5 * (xslices[1:] + xslices[:-1])
        ydata = 0.5 * (yslices[1:] + yslices[:-1])
        xv, yv = np.meshgrid(xdata, ydata)
        x, y, = xv.ravel(), yv.ravel()
        z = zvals.T.ravel()

        # Generate heat map and colour bar
        fig, ax = plt.subplots()
        h = ax.hist2d(x, y, weights = z, bins = (xslices, yslices),
                      cmin = 1, cmap = cmap,
                      norm = mplc.LogNorm() if logz else None)
        cb = fig.colorbar(h[3], ax = ax)

        # Formatting
        ax.set_aspect('equal', adjustable = 'box') # make the main plot a square
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        cb.set_label(cblabel)
        
    # ----------------------- Fitting methods ----------------------
    
    def gen_min_func(self, R2s, drifts, R_min, R_max, dt_min, dt_max):

        """
        Generic minimisation function for a generic volume with
        minimum and maximum radii (R_min and R_max) and drift time 
        bounds (dt_min and dt_max). Evaluates the sensitivity
        metric for a set of (R2, drift time) data points.
        """

        # Count how many events fall within this volume
        counts = np.count_nonzero((R2s <= R_max**2) & (R2s > R_min**2) \
                                & (drifts >= dt_min) & (drifts <= dt_max))

        # Estimate the mass of the volume
        mass = 7e3 * ((dt_max - dt_min) * (R_max**2 - R_min**2)) / (self.max_drift * 72.8**2)

        # Return negative of sensitivity metric (to minimize)
        S = -mass/np.sqrt(counts + 1) # add 1 to avoid edge cases
        return S
    
    def cylindrical_min_func(self, params, R2s, drifts, fixed_R = None):

        """
        For given parameters describing a cylinder (R_max, drift_low, drift_up),
        calculates a sensitivity metric M/sqrt(N), where M is the resulting
        fiducial mass and N is the number of events within the volume. If only
        the drift limits are provided, a fixed maximum radius should be given.
        """

        # Two parameter case, with only drift limits varied
        if len(params) == 2 and (fixed_R is not None):
            drift_low, drift_up = params
            R_max = fixed_R

        # Three parameter case
        elif len(params) == 3 and (fixed_R is None):
            R_max, drift_low, drift_up = params

        # Catch case
        else:
            print("Error: inputs are incorrect!")

        # Return 0 for non-sensical volumes (allow UDTs)
        if drift_low < 0 or R_max > 72.8:
            return 0

        # Count how many events fall within this cylinder
        counts = np.count_nonzero((R2s <= R_max**2) & (drifts >= drift_low) & (drifts <= drift_up))

        # Calculate the fiducial mass
        mass = 7e3 * ((drift_up - drift_low) * R_max**2) / (self.max_drift * 72.8**2)

        # Return negative of sensitivity metric (to minimize)
        S = -mass/np.sqrt(counts + 1) # add 1 to avoid edge cases
        return S
    
    def fit_wall_contour(self, R2s, drifts, bot_drift = 67.2, top_drift = 1044.3,
                         n_slices = 50, show_slices = False, verbose = False):

        """
        Takes a set of (R^2, drift time) points, and bins them into
        n_slices in drift time between specified maximum and minimum
        drift times. A unique cost function is constructed for each slice,
        such that a sensitivity metric S = -M_FV / sqrt(N_BG) is minimised
        for a given maximum radius for that point. Returns the drift time
        bin centres and the optimised radial values.
        """

        # Slice the detector in drift time
        n_slices = 50
        drift_slices = np.linspace(bot_drift, top_drift, n_slices + 1)
        drift_points = 0.5 * (drift_slices[1:] + drift_slices[:-1]) # slice centres

        # Loop over slices
        minima = []
        for i in range(n_slices):

            # Define a unique cost function with only the maximum R as a parameter
            costfunc = lambda R: self.cylindrical_min_func([R, drift_slices[i],
                                                            drift_slices[i + 1]], R2s, drifts) \
                             if (R < 72.8) and (R > 60) else 1e3

            # Minimise the cost function and append the result
            fit = fmin(costfunc, 70, disp = verbose)
            minima.append(fit[0])

            # Plot diagnostics
            if show_slices:

                # Create figure for drift-R^2 plot
                fig, ax = plt.subplots()

                # drift-R^2 plot
                h = ax.hist2d(R2s, -drifts, bins = 50,
                              range = ((0, 74**2), (-drift_slices[i + 1], -drift_slices[i])),
                              cmin = 1, cmap = 'cividis')

                # Mark the best-fit maximum radius
                ax.plot(fit[0]**2, -drift_points[i], 'ro')
                ax.axvline(fit[0]**2, color = 'r', ls = ':')

                # Formatting
                cb = fig.colorbar(h[3], ax = ax)
                cb.set_label('Counts')
                ax.set_xlabel(r'R$^2$ [cm$^2$]')
                ax.set_ylabel(r'-Drift time [ms]')
                ax.set_xticks(np.array([10, 30, 40, 50, 60, 70])**2)
                ax.set_xticklabels([f'{r}' + r'$^{2}$' for r in [10, 30, 40, 50, 60, 70]])
                ax.set_title(f'Slice: {i + 1}/{n_slices}')
                plt.show()

        return drift_points, np.array(minima)
    
    # ------------------- General helper methods -------------------
    
    def cut_grouper(self, cuts):
    
        """
        For a list of cuts as boolean arrays,
        combines them into a single cut array
        through means of '&' statements.
        """

        return [all(c) for c in zip(*cuts)]
        
    def E_contour(self, E, S1_range, NR = False, npts = int(2e5)):
    
        """
        Produces a contour in S1-S2 space for a given energy, using
        provided reconstruction parameters. Added option for using keVnr.
        """

        # Create arrays of S1 and S2 values
        alpha, beta = 11, 1.1
        S1s = np.linspace(S1_range[0], S1_range[1], npts)
        S2s = (alpha * np.power(E, beta) - S1s/self.g1) * self.g2 \
              if NR else self.g2 * (E/self.W_keV - S1s/self.g1)
        
        # Set negative or zero S2s to very small values
        S2s[S2s <= 0] = 1
        
        return S1s, S2s
    
    def make_gif(self, image_strings, save_str, fps = 2):
        
        """
        Given a sorted list of image paths, creates
        a gif with the name save_str at the specified fps.
        """
        
        images = [imageio.imread(i) for i in image_strings]
        imageio.mimsave(self.fdir + f'plots/{save_str}.gif', images, fps = fps)
        
    @jit
    def graphical_cut(self, x, y, polygon):
    
        """
        For a given set of (x, y) cut vertices, returns a mask
        that applies a graphical cut such that only points
        within the constructed polygon are selected.
        """
        
        # Join datapoints together
        points = np.vstack((x, y)).T 
        
        # Construct polygon and produce mask
        p = Path(polygon)
        mask = p.contains_points(points)
        
        return mask
        
    def convert_UTC_to_MT(self, timestamp):
        
        """
        For a UNIX time stamp, converts it to a human-readable
        date and time format, and adjusts it into a Mountain Time
        zone.
        """
        
        # Convert time stamp to UTC
        utc_datetime = datetime.datetime.utcfromtimestamp(timestamp).replace(tzinfo = tz.tzutc())
        
        # Adjust time zone to Mountain Time and return time object
        mountain_tz = tz.gettz('America/Denver') # specify the Mountain Time (MDT) timezone
        mt_datetime = utc_datetime.astimezone(mountain_tz)
        return mt_datetime
        
    def load_skew_bands(self, stype = 'linear'):
    
        """
        For a given set of ER and NR skew bands in .txt files,
        parses the information for plots and cuts. Rows are
        expected in the form: [S1, band mean, [low_width, high_width]].
        The band points are set as member objects, and these are spline
        interpolated (with kind 'stype') to form functions for creating
        band cuts. Bands are produced in S1c-log10(S2c) format.
        """

        # General information
        bands_dir = '/global/cfs/cdirs/lz/physics/NEST_Bands/SR3/20230707/Flat/'
        ER_band_f = 'SR3_flatBeta_NEST_logS2_skew_band.txt'
        NR_band_f = 'SR3_flatNR_NEST_logS2_skew_band.txt'

        # Load and parse ER band
        ER_band = []
        with open(bands_dir + ER_band_f, 'r') as f:

            # Loop over lines and skip header
            lines = f.readlines()
            for i in range(1, len(lines)):

                # Format entries and append
                l = lines[i].split()
                ER_band.append([float(l[0]), float(l[1]),
                                float(l[1]) + float(l[2][1:]),
                                float(l[1]) - float(l[3][:-1])])

        # Load and parse NR band
        NR_band = []
        with open(bands_dir + NR_band_f, 'r') as f:

            # Loop over lines and skip header
            lines = f.readlines()
            for i in range(1, len(lines)):

                # Format entries and append
                l = lines[i].split()
                NR_band.append([float(l[0]), float(l[1]),
                                float(l[1]) + float(l[2][1:]),
                                float(l[1]) - float(l[3][:-1])])

        # Set the band points as member objects
        self.ER_band = np.array(ER_band)
        self.NR_band = np.array(NR_band)

        # Interpolate the bands and set them as member objects
        self.ER_band_func = {'mean': interp1d(self.ER_band[:, 0], self.ER_band[:, 1], kind = stype,
                                              fill_value = -1, bounds_error = False),
                             'high': interp1d(self.ER_band[:, 0], self.ER_band[:, 2], kind = stype,
                                              fill_value = -1, bounds_error = False),
                              'low': interp1d(self.ER_band[:, 0], self.ER_band[:, 3], kind = stype,
                                              fill_value = -1, bounds_error = False)}
        self.NR_band_func = {'mean': interp1d(self.NR_band[:, 0], self.NR_band[:, 1], kind = stype,
                                              fill_value = -1, bounds_error = False),
                             'high': interp1d(self.NR_band[:, 0], self.NR_band[:, 2], kind = stype,
                                              fill_value = -1, bounds_error = False),
                              'low': interp1d(self.NR_band[:, 0], self.NR_band[:, 3], kind = stype,
                                              fill_value = -1, bounds_error = False)}