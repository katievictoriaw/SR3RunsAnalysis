#ifndef CutsSR3RunsAnalysis_H
#define CutsSR3RunsAnalysis_H

#include "EventBase.h"
#include "SR3RunsAnalysisEvent.h"

class CutsSR3RunsAnalysis {

public:
    CutsSR3RunsAnalysis(SR3RunsAnalysisEvent* m_evt);
    ~CutsSR3RunsAnalysis();
    bool SR3RunsAnalysisCutsOK();

private:
    SR3RunsAnalysisEvent* m_evt;
};

#endif
