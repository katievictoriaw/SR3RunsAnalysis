#ifndef SR3RunsAnalysisEVENT_H
#define SR3RunsAnalysisEVENT_H

#include "EventBase.h"

#include <string>
#include <vector>

#include "RQs.h"

class SR3RunsAnalysisEvent : public EventBase {

public:
    SR3RunsAnalysisEvent(EventBase* eventBase);
    virtual ~SR3RunsAnalysisEvent();

    //  GET STRUCTURE AND VARIABLE TYPE FROM modules/rqlib/
    TTreeReaderValue<int> nPulses;
    TTreeReaderValue<vector<float>> tpcHGPulseArea;

private:
};

#endif // SR3RunsAnalysisEVENT_H
