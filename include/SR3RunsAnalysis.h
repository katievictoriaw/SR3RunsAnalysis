#ifndef SR3RunsAnalysis_H
#define SR3RunsAnalysis_H

#include "Analysis.h"

#include "CutsSR3RunsAnalysis.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

#include "SR3RunsAnalysisEvent.h"

class SkimSvc;

class SR3RunsAnalysis : public Analysis {

public:
    SR3RunsAnalysis();
    ~SR3RunsAnalysis();

    void Initialize();
    void Execute();
    void Finalize();
    
    // General variables
    double runID,
           eventID,
           triggerTimeStamp_s,
           triggerTimeStamp_ns,
           daq_livetime,
           trigger_livetime,
           physics_livetime,
           ETV_deadtime;
              
    // SS variables
    double SS_raw_S1,
           SS_raw_S2,
           SS_raw_S2_top,
           SS_raw_S2_bot,
           SS_S1c,
           SS_S2c,
           SS_S2c_top,
           SS_S2c_bot,
           SS_driftTime_us,
           SS_X_cm,
           SS_Y_cm,
           SS_R_cm,
           SS_cX_cm,
           SS_cY_cm,
           SS_cR_cm,
           SS_cZ_cm,
           SS_s1TBA;
    
    // SR1 cuts
    bool cxSR1_HSX,
         cxSR1_SPEX,
         cxSR1_S1rate,
         cxSR1_muonV,
         cxSR1_EA,
         cxSR1_HSC,
         cxSR1_stinger,
         cxSR1_S1shape,
         cxSR1_S1prom,
         cxSR1_S1timing,
         cxSR1_S1TBA,
         cxSR1_S2TBA,
         cxSR1_validXY,
         cxSR1_S2width,
         cxSR1_narrowS2,
         cxSR1_S2earlypeak,
         cxSR1_S2risetime,
         cxSR1_FCRXY,
         cxSR1_FV,
         cxSR1_burst_noise,
         cxSR1_buffer,
         cxSR1_skinV,
         cxSR1_skinVprompt,
         cxSR1_skinVdel,
         cxSR1_ODV,
         cxSR1_ODVprompt,
         cxSR1_ODVdel;
    
    // SR3 cuts
    bool cxSR3_HSX,
         cxSR3_SPEX,
         cxSR3_S1rate,
         cxSR3_muonV,
         cxSR3_ETV,
         cxSR3_HSC,
         cxSR3_stinger,
         cxSR3_S1TBA,
         cxSR3_S2TBA,
         cxSR3_S2XYquality,
         cxSR3_S2width,
         cxSR3_narrowS2,
         cxSR3_S2earlypeak,
         cxSR3_S2risetime,
         cxSR3_FCRXY,
         cxSR3_burst_noise,
         cxSR3_buffer,
         cxSR3_SSwindow,
         cxSR3_skinV,
         cxSR3_skinVprompt,
         cxSR3_skinVdel,
         cxSR3_ODV,
         cxSR3_ODVprompt,
         cxSR3_ODVdel;

protected:
    CutsSR3RunsAnalysis* m_cutsSR3RunsAnalysis;
    ConfigSvc* m_conf;
    SR3RunsAnalysisEvent* m_evt;
};

#endif
