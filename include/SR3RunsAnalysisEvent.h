#ifndef SR3RunsAnalysisEVENT_H
#define SR3RunsAnalysisEVENT_H

#include "EventBase.h"

#include <string>
#include <vector>

#include "RQs.h"

class SR3RunsAnalysisEvent : public EventBase {

public:
    SR3RunsAnalysisEvent(EventBase* eventBase);
    virtual ~SR3RunsAnalysisEvent();

    // General event RQs
    TTreeReaderValue<unsigned long> runID;
    TTreeReaderValue<unsigned long> eventID;
    TTreeReaderValue<unsigned long> triggerTimeStamp_s;
    TTreeReaderValue<unsigned long> triggerTimeStamp_ns;
    TTreeReaderValue<long> daq_livetime_s;
    TTreeReaderValue<long> daq_livetime_ns;
    TTreeReaderValue<long> trigger_livetime_s;
    TTreeReaderValue<long> trigger_livetime_ns;
    TTreeReaderValue<long> physics_livetime_s;
    TTreeReaderValue<long> physics_livetime_ns;
    
    // SS RQs
    TTreeReaderValue<int> SS_nSingleScatters;
    TTreeReaderValue<int> SS_s1PulseID;
    TTreeReaderValue<int> SS_s2PulseID;
    TTreeReaderValue<float> SS_s1Area;
    TTreeReaderValue<float> SS_s2Area;
    TTreeReaderValue<float> SS_correctedS1Area;
    TTreeReaderValue<float> SS_correctedS2Area;
    TTreeReaderValue<float> SS_s2BottomArea;
    TTreeReaderValue<float> SS_s2TopArea;
    TTreeReaderValue<float> SS_correctedS2BottomArea;
    TTreeReaderValue<float> SS_correctedS2TopArea;
    TTreeReaderValue<float> SS_driftTime_ns;
    TTreeReaderValue<float> SS_X_cm;
    TTreeReaderValue<float> SS_Y_cm;
    TTreeReaderValue<float> SS_cX_cm;
    TTreeReaderValue<float> SS_cY_cm;
    TTreeReaderValue<float> SS_cZ_cm;
    TTreeReaderValue<float> SS_s1TBA;

private:
};

#endif // SR3RunsAnalysisEVENT_H
