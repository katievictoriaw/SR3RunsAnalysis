# SR3RunsAnalysis

This is a generic ALPACA module designed to mainly process SR3 data. The intention for this module is to facilitate analyses in python; the output file contains TTrees filled with basic RQs, as well as the full selection of both SR1 and SR3 core cuts wherever possible.
